<?php
	if(!$_SERVER['HTTPS']== 'on'){
	$url_https="https://". $_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
	header("Location: $url_https");
	exit();
}
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<?php include_once('elements/head_content.php'); ?>
	</head>  
	<body>
		<h1><span>TU SALTO SAMBA</span></h1>
		<!-- inicio HEADER -->
		<!-- fin NAV -->
		<?php include_once('elements/header.php'); ?>
		<!-- fin HEADER -->
		<!-- inicio NAV -->
		<?php include_once('elements/nav.php'); ?>
		<!-- inicio SECTION -->
		<?php include_once('elements/section.php'); ?>
		<!-- fin SECTION -->
		<!-- inicio FOOTER -->
		<?php include_once('elements/footer.php'); ?>
		<!-- fin FOOTER -->
		<!-- inicio LANDSCAPE -->
		<?php include_once('elements/landscape.php'); ?>
		<!-- fin LANDSCAPE -->
		
	</body>
</html>