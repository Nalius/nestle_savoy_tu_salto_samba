<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>jQuery UI Datepicker - Select a Date Range</title>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
  <!--link rel="stylesheet" href="../css/styles_admin.css"-->
</head>

<body>
  <div id="divtime" style="margin-top: 3%; position: fixed; margin-left: 5%;"> 
    <label for="from">DESDE: </label>
    <input type="text" id="from" name="from">
    <label for="to">HASTA:</label>
    <input type="text" id="to" name="to">
    <label for="to">REVISION DE CORREOS:</label>
    <input type="text" id="mail" name="mail">
    <p style= "cursor: pointer; width: 60px;" id="guardar" onclick="setTime();">Guardar</p>
  </div>
  <script>
    $(function() {
      $( "#from" ).datepicker({
        defaultDate: "+1w",
        changeMonth: true,
        numberOfMonths: 3,
        onClose: function( selectedDate ) {
          $( "#from" ).datepicker( "option", "minDate", selectedDate );
        }
      });
      $( "#to" ).datepicker({
        defaultDate: "+1w",
        changeMonth: true,
        numberOfMonths: 3,
        onClose: function( selectedDate ) {
          $( "#to" ).datepicker( "option", "maxDate", selectedDate );
        }
      });
      $( "#mail" ).datepicker({
        defaultDate: "+1w",
        changeMonth: true,
        numberOfMonths: 3,
        onClose: function( selectedDate ) {
          $( "#mail" ).datepicker( "option", "minDate", selectedDate );
        }
      });
    });
  </script> 
</body>

</html>