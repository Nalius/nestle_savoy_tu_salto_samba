$(document).ready(function(){
    //ejecucion de funciones al cargar la pagina
    menu();
    cambiardiv(1);   
    cambiardiveCinco(1);
    countDown();  
    showWinner();
    $ ("#fechanac").mask ("99/99/9999", {placeholder: 'dd/mm/yyyy'});    //mascara de fecha obligatoria dd/mm/aaaa
    
    //fb async init 
    (function(d){
       var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
       if (d.getElementById(id)) {return;}
       js = d.createElement('script'); js.id = id; js.async = true;
       js.src = "https://connect.facebook.net/en_US/all.js";
       ref.parentNode.insertBefore(js, ref);
    }(document));
    // Iniciamos el sdk y su configuración
    window.fbAsyncInit = function() {
      FB.init({
        appId    : '930747070309049',// la appid de tu aplicación facebook
        status   : true,
        cookie   : true,
        xfbml    : true,
        oauth    : true // habilita oauth 2.0
      });
      fbLog();
    } 


function fillData(fid,fname,lname,email,gender){
    var data = "fid="+fid+"&fname="+fname+"&lname="+lname+"&email="+email;
    $('#nombres').val(fname);
    $('#apellidos').val(lname);
    $('#email').val(email);
    if(gender=='female'){
        $('#fem').attr('checked',true);
        $('.gender').text('Femenino');
    }else{
        $('#masc').attr('checked',true);
        $('.gender').text('Masculino');
    }
    $('#faceid').val(fid);
    $('.nombre2').text(fname+' '+lname);
    $('#idset').val(fid);
    $('.misdatos img').attr('src','https://graph.facebook.com/'+fid+'/picture?type=large');
    $.ajax({
        url: './inc/fillData.php',
        type: "POST",
        data: data, 
        beforeSend: function(){
            console.log('Ingresando');
        },
        success: function(resp){
            console.log(resp);
        }
    });    
}


function fbLog(){
    FB.login(function(){
        FB.api('/me/permissions', function (response) {
           console.log(response); 
           if(response && !response.error){
               console.log("Tienes permisos");           
               FB.api('/me', function(response) {
                   console.log(response.id);
                   console.log(response.first_name);
                   console.log(response.last_name);
                   console.log(response.email);
                   console.log(response.gender);
                   fillData(response.id,response.first_name,response.last_name,response.email,response.gender);
                });        

           }else{
               console.log("No Tienes permisos");  
           }
        });
    }, { scope: 'public_profile, email' });     
}


    if($(window).width() <= 480){
        $('nav').addClass('oculto');
        $('nav').hide();
    }else{
        $('nav').addClass('block');
        $('nav').removeClass('oculto');
        $('nav').show();
    }
    $(window).resize(function() {
        // This will execute whenever the window is resized
        if($(window).width() <= 480){
            $('nav').removeClass('block');  
            $('nav').addClass('oculto');
            $('nav').hide();
        }else{
            $('nav').removeClass('oculto');
            $('nav').show();
            $('nav').addClass('block'); 
        }
    });
});
function openShareWin (url) {
    var windowWidth = 800;
    var windowHeight = 600;
    var centerWidth = (window.screen.width - windowWidth) / 2;
    var centerHeight = (window.screen.height - windowHeight) / 2;
    newWindow = window.open(url, "SiteShare",'_blank', 'resizable=0,width=' + windowWidth + 
    ',height=' + windowHeight + 
    ',left=' + centerWidth + 
    ',top=' + centerHeight);
    newWindow.showModalDialog();
    window.open(this.href, this.target, 'width=300,height=400'); return false;
}

function insLogin(e){
    var id = $('#faceid').attr('value');
    var data = "id="+id;
    console.log(data);
    $.ajax({
        url: './app/instagram/login.php',
        type: "POST",
        data: data, 
        beforeSend: function(){
            //$('#preload').show();
            //console.log('Email');
        },
        success: function(resp){
            $('#preload').hide();
            console.log(resp);
            if(resp==1){
                rvins(e);
            }else{
                var windowWidth = 800;
                var windowHeight = 600;
                var centerWidth = (window.screen.width - windowWidth) / 2;
                var centerHeight = (window.screen.height - windowHeight) / 2;
                newWindow = window.open(resp, '_blank', 'resizable=0,width=' + windowWidth + 
                ',height=' + windowHeight + 
                ',left=' + centerWidth + 
                ',top=' + centerHeight);
                if(!newWindow){
                    $('#ifrm').text('Debes permitir ventanas emergentes para votar por Instagram')
                    $( "#dialog" ).dialog();
                }
            }
        }
    });            
}

function sendMail(){
    var id = $('#faceid').attr('value');
    var data = "id="+id;
    console.log(data);
    $.ajax({
        url: './inc/PHPMailer.php',
        type: "POST",
        data: data, 
        beforeSend: function(){
            //$('#preload').show();
            console.log('Email');
        },
        success: function(resp){
            $('#preload').hide();
            console.log(resp);
        }
    });    
}
function validarsalto(){
    var id = $('#faceid').attr('value');
    console.log(id);
    var data = 'id='+id;
    $.ajax({
        url: './inc/validar_salto.php',
        type: "POST",
        data: data, 
        beforeSend: function(){
            $('#preload').show();
            console.log('validando');
        },
        success: function(resp){
            $('#preload').hide();
            console.log(resp);
            if(resp == 1){
                $('#tres').hide();
                $('#seis').show();
            }else{
                $('#tres').show();
                $('#seis').hide();
            }
        }
    });   
}

function validarsaltoDos(){
    var id = $('#faceid').attr('value');
    console.log(id);
    var data = 'id='+id;
    $.ajax({
        url: './inc/validar_salto.php',
        type: "POST",
        data: data, 
        beforeSend: function(){
            $('#preload').show();
            console.log('validando');
        },
        success: function(resp){
            $('#preload').hide();
            console.log(resp);
            if(resp == 1){
                $('#uno').hide();
                $('#dos').show();
                $('#tres').hide();
            }else{
                $('#uno').show();
                $('#dos').hide();
                $('#tres').hide();
            }
        }
    });   
}

function validarPerfil(){
    var id = $('#faceid').attr('value');
    console.log(id);
    var data = 'id='+id;
    $.ajax({
        url: './inc/validar_salto.php',
        type: "POST",
        data: data, 
        beforeSend: function(){
            //$('#preload').show();
            console.log('validando');
        },
        success: function(resp){
            $('#preload').hide();
            console.log(resp);
            if(resp == 1){
                $('#tres').hide();
                $('#seis').hide();
                $('#cuatro').show();
            }else{
                $('#tres').show();
                $('#seis').hide();
                $('#cuatro').hide();
            }
        }
    });   
}

//Desde aqui comienzan las funciones
function menu(){
    if($(window).width() <= 480){
        $('.btn_menu').click(function(event) {
            if ($('nav').hasClass('oculto')){
                $('nav').show();
                $('nav').removeClass('oculto');
                $('nav').addClass('activo');
            }else {
                $('nav').hide();
                $('nav').removeClass('activo');
                $('nav').addClass('oculto');
            }
        });
    }
}
function closemenu(){
    if ($('nav').hasClass('activo')){
            $('nav').hide();
            $('nav').removeClass('activo');
            $('nav').addClass('oculto');
    }
}
function nuevoAjax(){
    var xmlhttp=false;
    try {
        xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
    } catch (e) {
        try {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        } catch (E) {
            xmlhttp = false;
        }
    }
    if (!xmlhttp && typeof XMLHttpRequest!='undefined') {
        xmlhttp = new XMLHttpRequest();
    }
    return xmlhttp;
}
function cambiar(li, e){
    if($('li').removeClass("activo")){
        $('li.'+li).addClass("activo");
    }
    if(e == null){
        e = 1;
    }
    ajax = nuevoAjax();
    ajax.open('POST', './elements/contents/view/etapa_'+e+'/content.php',true);
    ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    ajax.send("contenido="+li);
    ajax.onreadystatechange = function() {
        if (ajax.readyState == 4) {
            document.getElementById('contenido').innerHTML = ajax.responseText;
        }
    }
}
function cambiardiv(div){

    if($('li').removeClass("activo")){
        $('li.'+div).addClass("activo");
        closemenu();
    }
    switch(div){
        case 1:
            $('#uno').show();
            $('#dos').hide();
            $('#tres').hide();
            $('#cuatro').hide();
            $('#seis').hide();
            break;
        case 2:
            setTimer();
            $('#uno').hide();
            $('#dos').show();
            $('#tres').hide();
            $('#cuatro').hide();
            $('#seis').hide();
            //quitar();
            break;
        case 3:
            $('#uno').hide();
            $('#dos').hide();
            $('#cuatro').hide();
            validarsalto();
            break;
        case 4:
            $('#uno').hide();
            $('#dos').hide();
            $('#tres').hide();
            $('#cuatro').show();
            $('#seis').hide();
            validarPerfil();  
            showimgperfil();
            break;
        case 5:
            $('#uno').hide();
            $('#dos').hide();
            $('#tres').hide();
            $('#cuatro').show();
            $('#seis').hide();
            validarPerfil();   
        break;             
    }
}
function cambiardivedos(div){
    if($('li').removeClass("activo")){
        $('li.'+div).addClass("activo");
        closemenu();
    }
    switch(div){
        case 1:
            $('#uno').show();
            $('#dos').hide();
            $('#tres').hide();
            break;
        case 2:
            $('#uno').hide();
            $('#dos').show();
            $('#tres').hide();
            validarsaltoDos();
            showimgperfil();
            break;
        case 3:
            $('#uno').hide();
            $('#dos').hide();
            $('#tres').show();
            busca_todos();
            break;
    }
}

function cambiardiveTres(div){
    if($('li').removeClass("activo")){
        $('li.'+div).addClass("activo");
        closemenu();
    }
    switch(div){
        case 1:
            $('#uno').show();
            $('#dos').hide();
            $('#tres').hide();
            break;
        case 2:
            $('#uno').hide();
            $('#dos').show();
            $('#tres').hide();
            busca_todos_et3();
            break;
        case 3:
            $('#uno').hide();
            $('#dos').hide();
            $('#tres').show();
            break;
    }
}


function cambiardiveCuatro(div){
    if($('li').removeClass("activo")){
        $('li.'+div).addClass("activo");
        closemenu();
    }
    switch(div){
        case 1:
            $('#uno').show();
            $('#dos').hide();
            $('#tres').hide();
            break;
        case 2:
            $('#uno').hide();
            $('#dos').show();
            $('#tres').hide();
            busca_todos_et4();
            break;
        case 3:
            $('#uno').hide();
            $('#dos').hide();
            $('#tres').show();
            break;
    }
}

function cambiardiveCinco(div){
  if($('li').removeClass("activo")){
        $('li.'+div).addClass("activo");
        closemenu();
    }
        switch(div){
            case 1:
                $('#uno').show();
                $('#dos').hide();
                $('#tres').hide();
                $('#cuatro').hide();
                $('#seis').hide();
                break;
            case 2:
                setTimer();
                $('#uno').hide();
                $('#dos').show();
                $('#tres').hide();
                $('#cuatro').hide();
                $('#seis').hide();
                //quitar();
                break;
            case 3:
                $('#uno').hide();
                $('#dos').hide();
                $('#tres').show();
                $('#cuatro').hide();
                break;
            case 4:
                $('#uno').hide();
                $('#dos').hide();
                $('#tres').hide();
                $('#cuatro').show();
                $('#seis').hide();
                break;     
        }
    }


function showVideo(e){
    var data="id="+e;
     $.ajax({
        url: './inc/links.php',
        type: "POST",
        data: data, 
        beforeSend: function(){
            //$('#preload').show();
            console.log('Buscando Video');
        },
        success: function(resp){
            console.log(resp);
            if(resp != '0'){
                var windowWidth = 800;  
                var windowHeight = 600;
                var centerWidth = (window.screen.width - windowWidth) / 2;
                var centerHeight = (window.screen.height - windowHeight) / 2;
                newWindow = window.open(resp, '_blank', 'resizable=0,width=' + windowWidth + 
                ',height=' + windowHeight + 
                ',left=' + centerWidth + 
                ',top=' + centerHeight);  
                if(!newWindow){
                    alert('Debes permitir ventanas emergentes para votar por Instagram');
                }
            }    
        }
    });  

}

function showimgperfil(){
    var id = $('#faceid').attr('value');
    console.log(id);
    var data = 'id='+id;
    $.ajax({
        url: './inc/ajax_upload/sql_select.php',
        type: "POST",
        data: data, 
        beforeSend: function(){
            //$('#preload').show();
            console.log('Buscando Perfil');
        },
        success: function(resp){
            $('#preload').hide();
            resp = resp.split(',');
            console.log(resp);
            $("#img1").attr('src',resp[0]);
            $("#img2").attr('src',resp[1]);
            $("#img3").attr('src',resp[2]);
            $("#img4").attr('src',resp[3]);
            $("#salto").text(resp[4]);
            $(".misalto").children("img").attr('src',resp[5]);
            $("#edad").text(resp[6]+' años');      
        }
    });
}


function showWinner(){
    var data;
    $.ajax({
        url: './inc/resultados_ganador.php',
        type: "POST",
        data: data, 
        beforeSend: function(){
            $('#preload').show();
            console.log('Mostrando Ganador');
        },
        success: function(resp){
            $('#preload').hide();
            console.log(resp);
            resp= resp.split('/');
            $('#name_winner').text(resp[0]);
            $('.svotos').text(resp[1]);
        }
    });
}

function setTimer(){
    var data;
    $.ajax({ 
        url: './inc/consultaTiempo.php',
        type: "POST",
        data: data, 
        beforeSend: function(){
            console.log('Consultando fecha');
        },
        success: function(resp){  
            resp = resp.split('-'); 
            console.log(resp[0]);  
            $('.mod_comosalto li').eq(4).children('span').text(resp[0]);
            $('.mod_comosalto li').eq(5).children('span').eq(1).text(resp[1]);
            $('.mod_comosalto li').eq(5).children('span').eq(2).text(resp[0]);
        }
    });    
}        

setInterval(function(){countDown();},59000);
function countDown(){
    var data;
    $.ajax({ 
        url: './inc/consultaTiempo.php',
        type: "POST",
        data: data, 
        beforeSend: function(){
            console.log('Consultando fecha');
        },
        success: function(resp){  
            resp = resp.split('-'); 
            console.log(resp[0]);
            var Arresp = resp[0].split('/');
            console.log(Arresp);

        var fecha=new Date(Arresp[2],(Arresp[1]-1),Arresp[0]);
        console.log(fecha);
        var hoy=new Date();
        var dias=0;
        var horas=0;
        var minutos=0;
        var segundos=0;

        if (fecha>hoy){
            var diferencia=(fecha.getTime()-hoy.getTime())/1000;
            dias=Math.floor(diferencia/86400);
            diferencia=diferencia-(86400*dias);
            horas=Math.floor(diferencia/3600);
            diferencia=diferencia-(3600*horas);
            minutos=Math.floor(diferencia/60);
            diferencia=diferencia-(60*minutos);
            segundos=Math.floor(diferencia);  
        }    

        $('#dia').text(dias+' '+'Dias');
        $('#hora').text(horas+' '+'Horas');
        $('#min').text(minutos+' '+'Minutos');
        }  
    });  
}

function femclick(){
    $("#masc").attr('checked',false);       
}

function masclick(){
    $("#fem").attr('checked',false);        
}

function showtycv(id){
    $(id).show();
}
function hidetycv(id){
    $(id).hide();
}
function redirect(id){
    $(id).hide();
    cambiardiv(4);
}

function sendform(){
    var id = $('#faceid').attr('value');
    var img1 = $("#im1").attr('src');
    var img2 = $("#im2").attr('src');
    var img3 = $("#im3").attr('src');
    var img4 = $("#im4").attr('src');
    var birth= $("#fechanac").attr('value');
    var address  = $("#direccion").attr('value').trim();
    var month    = $("#fechanac").val().trim().split('/');
    var nombre   = $("#nombres").val().trim();
    var apellido = $("#apellidos").val().trim();
    var email    = $("#email").val().trim();
    var tyc      = $("#tyc").attr('checked'); 
    var gender   = '';

    if($("#masc").attr('checked')){
        gender = 'm';
    }else{
        gender = 'f';
    }

    if (birth=='' || address=='' || month=='' ||  apellido=='' || email=='' || (!tyc)){
        alert('Debe completar todos los campos');
        return;
    }

    if(Number(month[1])>12){
        alert("Fecha Invalida, Favor verificar dd/mm/aaaa");
        return;
    }
    
    if(img1=="images/plus.png" || img2=="images/plus.png" || img3=="images/plus.png" || img4=="images/plus.png"){
        $('#faltafoto').show();
        return;

    }       
    var data = "id="+id+"&nombre="+nombre+"&apellido="+apellido+"&gender="+gender+"&birth="+birth+"&address="+address+"&email="+email+'&img1='+img1+'&img2='+img2+'&img3='+img3+'&img4='+img4;
    $.ajax({ 
        url: './inc/ajax_upload/sql_insert.php',
        type: "POST",
        data: data, 
        beforeSend: function(){
            $('#preload').show();
            console.log('enviando datos a la BD');
        },
        success: function(resp){   
            $('#preload').hide();
            console.log(resp);
            validarsalto();
        }
    });
}
//buscardor de seccion2--> votaporulsato.php
function buscar(){
    var nombre = $('#search').attr('value');
    console.log(nombre);    
    data= 'nombre='+nombre;
    consulta_nombre(data);
}

function consulta_nombre(data){
    $.ajax({ 
        url: 'inc/ajax_upload/consultanombre_e2_sql.php',
        type: "POST",
        data: data, 
        beforeSend: function(){
            //$('#preload').show();
            console.log('Consultado...');
        },
        success: function(resp){
            $('#preload').hide();
            console.log('Listo');
            if(data.lenght==0){
                $('#found').empty();
                $('#found').hide();               
                $('#no_found').show();
            }

            if(resp==0){
                $('#found').empty();
                $('#found').hide();
                $('#no_found').show();
            }else{
                $('#found').empty();
                $('#found').show();
                $('#no_found').hide();
                $('#found').append(resp);
            }    
        }
    });
}  

function busca_todos(){    
     $.ajax({ 
        url: 'inc/resultados_e2.php',
        type: "POST",
       // data: data, 
        beforeSend: function(){
            $('#preload').show();
            console.log('Consultado...');
        },
        success: function(resp){
            $('#preload').hide();   
            console.log('Listo');
            console.log(resp);
            $('#found').empty();
            $('#no_found').hide();
            $('#found').append(resp);
            $('#found').show();
        },
        complete: function(){
          
        }   
    });
}

function busca_todos_et3(){
    var data;
    $.ajax({ 
        url: 'inc/resultados_e3.php',
        type: "POST",
        data: data, 
        beforeSend: function(){
            $('#preload').show();
            console.log('Consultado...');
        },
        success: function(resp){
            $('#preload').hide();   
            console.log('Listo');
            console.log(resp);
            $('#found').empty();
            $('#no_found').hide();
            $('#found').append(resp);
            $('#found').show();
        },
        complete: function(){
          
        }   
    });
}

function busca_todos_et4(){
    var data;
    $.ajax({ 
        url: 'inc/resultados_e4.php',
        type: "POST",
        data: data, 
        beforeSend: function(){
            $('#preload').show();
            console.log('Consultado...');
        },
        success: function(resp){
            $('#preload').hide();   
            console.log('Listo');
            console.log(resp);
            $('#no_found').hide();
            $('#found').empty();
            $('#found').append(resp);
            $('#found').show();
        },
        complete: function(){
          
        }   
    });
}

function buscar_et3(){
    var nombre = $('#search').attr('value');
    console.log(nombre);    
    data= 'nombre='+nombre;
    consulta_nombre_et3(data);
}

function buscar_et4(){
    var nombre = $('#search').attr('value');
    console.log(nombre);    
    data= 'nombre='+nombre;
    consulta_nombre_et4(data);
}

function consulta_nombre_et3(data){
    $.ajax({ 
        url: 'inc/ajax_upload/consultanombre_e3_sql.php',
        type: "POST",
        data: data, 
        beforeSend: function(){
            //$('#preload').show();
            console.log('Consultado...');
        },
        success: function(resp){  
        $('#preload').hide(); 
            console.log('Listo');

            if(data.lenght==0){
                $('#found').empty();
                $('#found').hide();               
                $('#no_found').show();
            }

            if(resp==0){
                $('#found').empty();
                $('#found').hide();
                $('#no_found').show();
            }else{
                $('#found').empty();
                $('#found').show();
                $('#no_found').hide();
                $('#found').append(resp);
            }           
        }
    });
}

function consulta_nombre_et4(data){
    $.ajax({ 
        url: 'inc/ajax_upload/consultanombre_e4_sql.php',
        type: "POST",
        data: data, 
        beforeSend: function(){
            //$('#preload').show();
            console.log('Consultado...');
        },
        success: function(resp){  
        $('#preload').hide(); 
            console.log('Listo');

            if(data.lenght==0){
                $('#found').empty();
                $('#found').hide();               
                $('#no_found').show();
            }

            if(resp==0){
                $('#found').empty();
                $('#found').hide();
                $('#no_found').show();
            }else{
                $('#found').empty();
                $('#found').show();
                $('#no_found').hide();
                $('#found').append(resp);
            }           
        }
    });
}


function consulta_nombre_et2(data){
    $.ajax({ 
        url: 'inc/ajax_upload/consultanombre_e2_sql.php',
        type: "POST",
        data: data, 
        beforeSend: function(){
            //$('#preload').show();
            console.log('Consultado...');
        },
        success: function(resp){  
        $('#preload').hide(); 
            console.log('Listo');

            if(data.lenght==0){
                $('#found').empty();
                $('#found').hide();               
                $('#no_found').show();
            }

            if(resp==0){
                $('#found').empty();
                $('#found').hide();
                $('#no_found').show();
            }else{
                $('#found').empty();
                $('#found').show();
                $('#no_found').hide();
                $('#found').append(resp);
            }           
        }
    });
}

function uploadimg(e){
    $("#up").val(e);
    if($("input[name='upload"+e+"']:hidden").trigger( "click")){
       $("input[name='upload"+e+"']").on("change",function(){
            var formData = new FormData($("#formulario")[0]);
            var ruta = "inc/ajax_upload/img_upload.php";
            $.ajax({
                url: ruta,
                type: "POST",
                data: formData,
                contentType: false,
                processData: false,
                beforeSend: function(){
                    $("#im"+e).attr('src', './images/ajaxSpinner.gif');
                    console.log('Consultado...');
                },
                success: function(datos)
                {                   
                        console.log(datos);
                        $("#im"+e).attr('src',datos.substring(4));
                        $("input[name='upload"+e+"']").val("");

                        //alert(datos.substring(4));                                                                 
                }
            });
        });
    }
    
}

function compartir(){
   var ima1=  $(".misalto").children("img").attr('src');
   var jump=  $("#salto").text();
   FB.login(function(){}, { scope: 'public_profile, email' });
   FB.api('/me/permissions', function (response) {

       console.log(response); 
       if(response && !response.error){
           console.log("Tienes permisos");
           FB.ui({
               method: 'feed',
               link: 'https://www.facebook.com/desarrollolawebtest/app_930747070309049?ref=page_internal',
               picture : 'https://hacemosloquenosgusta.com/extras/SaltoSamba/'+ima1,
               caption: 'Tu Salto Samba',
               description: 'Yo elegi: "'+jump+'" como mi Salto Samba',
               name: 'Nestle Samba',
            }, function(response){});
           FB.api('/me', function(response) {
               console.log(response.id);
               console.log('https://hacemosloquenosgusta.com/extras/SaltoSamba/'+ima1);
           });      
       }else{
           console.log("No Tienes permisos");  
       }
   });
}


function shareVote(){ 
    var ima1= $('.topefondo').attr('src');
    FB.login(function(){}, { scope: 'public_profile, email' });
    FB.api('/me/permissions', function (response) {
       console.log(response); 
       if(response && !response.error){
           console.log("Tienes permisos");
           FB.ui({
               method: 'feed',
               link: 'https://www.facebook.com/desarrollolawebtest/app_930747070309049?ref=page_internal',
               picture : 'https://hacemosloquenosgusta.com/extras/SaltoSamba/'+ima1,
               caption: 'Tu Salto Samba',
               description: 'Yo ayude a un amigo a dar su Salto Samba',
               name: 'Nestle Samba',
            }, function(response){});
           FB.api('/me', function(response) {
               console.log(response.id);
               console.log('https://hacemosloquenosgusta.com/extras/SaltoSamba/'+ima1);
           });      
       }else{
           console.log("No Tienes permisos");  
       }
    });

}

function rvfb(e){
    //alert('click');
    var fbid  = $('#faceid').attr('value');
    var data  = 'id='+e+'&fbid='+fbid;
    $.ajax({
        url: "inc/ajax_upload/registra_voto_facebook.php",
        type: "POST",
        data: data,
        success: function(datos)
        {                   
            console.log(datos);
            if(datos=='0'){ 
                alert('Usted ya ha votado por este candidato a traves de Facebook');     
                return;                                                    
            }else{
               $('#velo_votaste').show();               
            }              
        }
    });
}

function rvins(e){
    var fbid  = $('#faceid').attr('value');
    var data  = 'id='+e+'&fbid='+fbid;
    $.ajax({
        url: "inc/ajax_upload/registra_voto_instagram.php",
        type: "POST",
        data: data,
        success: function(datos)
        {                   
            console.log(datos);
            if(datos=='0'){ 
                alert('Usted ya ha votado por este candidato a traves de Instagram');     
                return;                                                    
            }else{
                $('#velo_votaste').show();                     
            }                                                          
        }
    });
}

function showVeloVoto(e){
    var img = $(e).parent('#salto-1 img').attr('src');
    var pers= $(e).parent('#salto-1 .name').text();
    $('#velo_votaste').show();
}
//Tooltips

    var changeTooltipPosition = function(event) {
      var tooltipX = event.pageX - 8;
      var tooltipY = event.pageY + 8;
      $('div.tooltip').css({top: tooltipY, left: tooltipX});
    };
 
    var showTooltip = function(event) {
      $('div.tooltip').remove();
      $('<div class="tooltip">I\' am tooltips! tooltips! tooltips! :)</div>').appendTo('body');
      changeTooltipPosition(event);
    };
 
    var hideTooltip = function() {
       $('div.tooltip').remove();
    };
 
    $('p.txtpq').bind({
       mousemove : changeTooltipPosition,
       mouseenter : showTooltip,
       mouseleave: hideTooltip
    });