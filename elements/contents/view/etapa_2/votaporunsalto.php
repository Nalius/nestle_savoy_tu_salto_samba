<div class="mod_votar_salto">
    <p>Estamos en etapa de votaciones</p>
    <p>¡Ayúdalos a llegar a la meta!</p>
    <p>Las votaciones terminan en: 
        <span id="dia">88 DÍAS</span> / 
        <span id="hora">20 HORAS</span> / 
        <span id="min">1348 MINUTOS</span>
    </p>
<!-- inicio caja de busqueda-->   
    <div class="box_search">
        <div class="container-1">
            <span class="icon">
                <i class="fa fa-search"></i>
            </span>
            <span class="linea"></span>
            <input type="search" id="search" placeholder="Buscar..." onkeyup="buscar();">
        </div>
    </div>
<!-- fin caja de busqueda-->
<!--inicio resultado de la busquea -->   
    <div id="found"></div>
    <div id="no_found" style="display: none;">>
        <p>La persona que estás buscando <span>no se encuentra</span> dentro de los preseleccionados. </p>
        <p>¡Te invitamos a votar por otro SALTO!</p>
    </div>
<!--fin resultado de la busquea -->   
<!--inicio Velo-->
<div id="velo_votaste" class="velosalto votaste" style="display: none;">
    <a onclick="hidetycv(velo_votaste)"class="btn_cerrar">X</a>
    <div class="contenido_velo">
        <p>¡Excelente!</p>
        <p>Tu voto ya ha sido registrado</p>
        <p>¡Gracias por ayudarlo a llegar a la meta!</p>
        <a href="#" class="btn_azul izquierda velo" onclick="shareVote();">Compartir</a>
        <a onclick="hidetycv(velo_votaste)" class="btn_blanco velo cerrar">Cerrar</a>
    </div>
</div>
<!--fin Velo-->
<!--inicio Velo-->
<div id="velo_et2" class="velosalto etapados">
    <a onclick="hidetycv(velo_et2)" class="btn_cerrar">X</a>
    <div class="catdetalle">
        <div class="centrado"><img src="images/laptop.png" alt="fondotope" class="laptop"></div>
        <p class="txt-1">Si votas por tu salto favorito en INSTAGRAM <br/> 
            y sigues nuestra cuenta <span class="span-yellow">@SAMBAVENEZUELA</span> <br/> 
            estarás participando por una <span class="span-yellow">¡LAPTOP LENOVO!</span> </p>
        <p class="txt-2">¿Vas a dejar que otro se la gane? </p>
        <a onclick="hidetycv(velo_et2)" class="btn_blanco velo">¡Vamos! ¡a votar!</a>
        <p class="txt-3">ANUNCIAREMOS EL GANADOR POR NUESTRA CUENTA <br />
            DE INSTAGRAM <span class="span-yellow">@SAMBAVENEZUELA</span>  EL XX-XX-XXXX</p>
    </div>
</div>
<!--fin Velo-->
</div>