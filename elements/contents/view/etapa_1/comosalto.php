<div class="container">
   <div class="mod_comosalto">
        <p>Si tienes una <span>actividad soñada</span> que nunca has podido experimentar. . .</p>
        <p class="txtbg">¡ES TU <span>MOMENTO</span> DE REALIZARLA!</p>
        <li>Completa el perfil con tus datos y adjunta cuatro fotos que te describan.</li>
        <li>Luego, selecciona el SALTO que deseas realizar, ese que te quita el sueño, el que te haría realmente FELIZ. ¡Y LISTO!</li>
        <li>Espera la aprobación del Team SAMBA® en tu correo electrónico.</li>
        <li>Si nuestro Team te selecciona, solo deberás decirle a tus panas que voten por ti para entrar en la preselección y posteriormente ganarte el premio final.</li>
        <li>Tú tendrás hasta el <span>xx-xx-xx</span> para postular tu SALTO. Y una semana después del cierre de postulaciones publicaremos los 50 preseleccionados en Facebook e Instagram.</li>
        <li><span>Tus panas</span> tendrán desde el <span>xx-xx-xx</span> hasta el <span>xx-xx-xx</span> para votar por ti… Y también estarán concursando por un <span>premio.</span> ¿Qué tal?</li>
        <a onclick="cambiardiv(3);" class="btn_negro">Dar un salto</a>
    </div>
</div>