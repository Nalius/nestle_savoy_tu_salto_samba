<!-- INICIO VELO vrunner  -->
<div id="vrunner" class="velosalto less">
    <a onclick="hidetycv(vrunner)" class="btn_cerrar">X</a>
    <p class="txterror">Runner</p>
    <p class="txttyc">Si siempre has querido convertirte en un runner o corredor profesional, esta es tu</p>
	<p class="txttyc">oportunidad para lograrlo. Te damos el empujón inicial para que te entrenes por</p>
	<p class="txttyc">un día completo con corredores de primera, con alta experiencia y ganas de</p>
	<p class="txttyc">ayudarte a llegar al final de la meta. </p>
    <a href="#" onclick="selectsalto('Runner', vrunner)" class="btn_blanco">Seleccionar</a>
</div>
<!-- FIN VELO vrunner -->
<!-- INICIO VELO vflyboarding  -->
<div id="vflyboarding" class="velosalto less">
    <a onclick="hidetycv(vflyboarding)" class="btn_cerrar">X</a>
    <p class="txterror">Flyboarding</p>
    <p class="txttyc">Si los deportes acuáticos son lo tuyo y quieres acercarte mucho más, o por </p>
	<p class="txttyc">primera vez a esta práctica, esta es tu opción. El Flyboarding hace realidad tu </p>
	<p class="txttyc">sueño de volar sobre el agua, con la libertad de crear movimientos a tu estilo, </p>
	<p class="txttyc">mientras disfrutas de un día de playa. </p>
    <a href="#" onclick="selectsalto('Flyboarding', vflyboarding)" class="btn_blanco">Seleccionar</a>
</div>
<!-- FIN VELO vflyboarding -->
<!-- INICIO VELO vflyboarding  -->
<div id="vscubadiver" class="velosalto less">
    <a onclick="hidetycv(vscubadiver)" class="btn_cerrar">X</a>
    <p class="txterror">Scuba Diver</p>
    <p class="txttyc">¡Sumérgete a lo grande! Y da el primer paso para convertirte en un buzo certificado. </p>
    <p class="txttyc">Durante este día, serás capaz de bucear hasta una profundidad </p>
	<p class="txttyc">máxima de 12 metros con la supervisión de un profesional. ¿Malo? Sabes que  </p>
	<p class="txttyc">siempre lo has querido y te damos la SAMBA® para lograrlo.</p>
    <a href="#" onclick="selectsalto('Scuba Diver', vscubadiver)" class="btn_blanco">Seleccionar</a>
</div>
<!-- FIN VELO vflyboarding -->
<!-- INICIO VELO vfotografo  -->
<div id="vfotografo" class="velosalto less">
    <a onclick="hidetycv(vfotografo)" class="btn_cerrar">X</a>
    <p class="txterror">Fotógrafo</p>
    <p class="txttyc">Si no pierdes la oportunidad para tomar una foto, es el momento de ir más allá y </p>
	<p class="txttyc">conocer todo lo necesario para tomar las más brutales. SAMBA® te permitirá </p>
	<p class="txttyc">manejar por un día el mejor equipo fotográfico para que salgas a fotear con </p>
	<p class="txttyc">profesionales y vivas al máximo la experiencia a través del lente de tu cámara. </p>
    <a href="#" onclick="selectsalto('Fotógrafo', vfotografo)" class="btn_blanco">Seleccionar</a>
</div>
<!-- FIN VELO vfotografo -->
<!-- INICIO VELO vDj  -->
<div id="vDj" class="velosalto less">
    <a onclick="hidetycv(vDj)" class="btn_cerrar">X</a>
    <p class="txterror">Dj</p>
    <p class="txttyc">Si te la pasas soñando con un plato y con hacer scratch todo el día, tienes ahora </p>
	<p class="txttyc">la oportunidad de dar el primer salto para convertirte en un Dj profesional con </p>
	<p class="txttyc">mucha SAMBA®. En un día, te daremos las primeras herramientas para que </p>
	<p class="txttyc">arranques y lo logres. Si esto es lo tuyo, ¡SALTA! </p>
    <a href="#" onclick="selectsalto('Dj', vDj)" class="btn_blanco">Seleccionar</a>
</div>
<!-- FIN VELO vDj -->
<!-- INICIO VELO vlocucion  -->
<div id="vlocucion" class="velosalto less">
    <a onclick="hidetycv(vlocucion)" class="btn_cerrar">X</a>
    <p class="txterror">Locución</p>
    <p class="txttyc">¿Siempre te dicen que tienes buena voz y tremenda dicción? Si es así y te gusta la </p>
	<p class="txttyc">idea de estar detrás de un micrófono creando y difundiendo contenidos </p>
	<p class="txttyc">increíbles, SAMBA® te da la oportunidad de visitar una cabina de radio en </p>
	<p class="txttyc">compañía de los mejores locutores del país, para que muchos otros te escuchen. </p>
    <a href="#" onclick="selectsalto('Locución', vlocucion)" class="btn_blanco">Seleccionar</a>
</div>
<!-- FIN VELO vlocucion -->
<!-- INICIO VELO vsurf  -->
<div id="vsurf" class="velosalto less">
    <a onclick="hidetycv(vsurf)" class="btn_cerrar">X</a>
    <p class="txterror">Surf</p>
    <p class="txttyc">Si quieres iniciarte en el mundo del Surf, este es tu momento. Queremos darte todo </p>
	<p class="txttyc">lo necesario para que conozcas y aprendas de este increíble deporte. Sumérgete </p>
	<p class="txttyc">y corre olas increíbles en un día extremo con los mejores instructores de surf y los </p>
	<p class="txttyc">paisajes con más SAMBA® del país.</p>
    <a href="#" onclick="selectsalto('Surf', vsurf)" class="btn_blanco">Seleccionar</a>
</div>
<!-- FIN VELO vsurf -->
<!-- INICIO VELO vbaterista  -->
<div id="vbaterista" class="velosalto less">
    <a onclick="hidetycv(vbaterista)" class="btn_cerrar">X</a>
    <p class="txterror">Baterista</p>
    <p class="txttyc">Si eres de los que tiene un oído musical ultra desarrollado y el ritmo es tu mejor </p>
	<p class="txttyc">don! SAMBA® te dará  lo que necesitas para dar tus primeros baquetazos con la  </p>
	<p class="txttyc">ayuda de un excelente baterista. Conviértete en un Rock Star y SALTA si la música </p>
	<p class="txttyc">es lo que te mueve.</p>
    <a href="#" onclick="selectsalto('Baterista', vbaterista)" class="btn_blanco">Seleccionar</a>
</div>
<!-- FIN VELO vbaterista -->
<!-- INICIO VELO vdoblaje  -->
<div id="vdoblaje" class="velosalto less">
    <a onclick="hidetycv(vdoblaje)" class="btn_cerrar">X</a>
    <p class="txterror">Doblaje</p>
    <p class="txttyc">Si te gusta imitar personajes todo el tiempo y siempre has querido que tu voz le de </p>
	<p class="txttyc">vida a alguno de ellos este es tu SALTO. Te daremos un día completo de mucha </p>
	<p class="txttyc">actuación y canto para que aprendas técnicas básicas para doblar voces y que </p>
	<p class="txttyc">lo hagas increíble. Así que si eres extrovertido ¡SALTA! </p>
    <a href="#" onclick="selectsalto('Doblaje', vdoblaje)" class="btn_blanco">Seleccionar</a>
</div>
<!-- FIN VELO vboblaje -->
<!-- INICIO VELO vbaile  -->
<div id="vbaile" class="velosalto less">
    <a onclick="hidetycv(vbaile)" class="btn_cerrar">X</a>
    <p class="txterror">Baile</p>
    <p class="txttyc">Si tu sueño siempre ha sido convertirte en un bailarín profesional, ¡es tu momento! </p>
	<p class="txttyc">SAMBA® te dará la oportunidad para que aprendas a bailar como siempre lo </p>
	<p class="txttyc">imaginaste. En un día de mucho movimiento, aprende las primeras técnicas de </p>
	<p class="txttyc">baile de la mano de los mejores bailarines profesionales del país y conviértete en </p>
	<p class="txttyc">uno de ellos. </p>
    <a href="#" onclick="selectsalto('Baile', vbaile)" class="btn_blanco">Seleccionar</a>
</div>
<!-- FIN VELO vboblaje -->