<!-- INICIO VELO tyc  -->
<div id="tycv" class="velosalto" style="display: none;">
    <a onclick="hidetycv(tycv)" class="btn_cerrar">X</a>
	<p class="txterror">CONDICIONES Y REGLAMENTOS TU SALTO SAMBA®</p>
    <div class="overflow-y">
	    <p class="txttyc">-	Sólo, sin excepción, se aceptarán a los usuarios con edades comprendidas entre los 18 a 25 años. </p>
		<p class="txttyc">-	Para postularse, es necesario seguir todo el procedimiento, y en ello, incluyen los datos personales que se les exija. </p>
		<p class="txttyc">-	Se les proveerá de una semana para la postulación</p>
		<p class="txttyc">-	Los datos facilitados por cada concursante deben ser veraces. En caso de que fueran falsos y la persona resultase ganadora, automáticamente perderá el derecho a los premios que se detallan más adelante. </p>
		<p class="txttyc">-	Se recibirá 4 fotografías por persona (Fotografías que lo describan).</p>
		<p class="txttyc">-	Bajo ningún concepto, las fotografías puede contener mensajes con referencias políticas, religiosas, sexuales o que comprometan la dignidad humana. Aunado a que, no se pueden  hacer alusión directa o indirecta a otras marcas comerciales. </p>
		<p class="txttyc">-	Los usuarios que se postularon, deberán esperar por  la aprobación a través de su correo electrónico por parte del staff de SAMBA®. </p>
		<p class="txttyc">-	Después de una semana del cierre de postulaciones, se elegirán a 50 usuarios (que fueron postulados). </p>
		<p class="txttyc">-	El ganar en este SALTO SAMBA, dependerá, totalmente, del número de votaciones de los amigos del postulado. </p>
		<p class="txttyc">-	La participación en este concurso, supone la total aceptación de estas bases.</p>
	</div>
    <a onclick="hidetycv(tycv)" class="btn_blanco">OK</a>
</div>
<!-- FIN VELO tyc -->